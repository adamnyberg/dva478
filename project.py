import click
import pandas as pd
import glob
import numpy as np
import matplotlib.pyplot as plt
# import seaborn as seabornInstance
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import Pipeline
from sklearn.model_selection import cross_val_score

plt.rcParams.update({'font.size': 22})


DATA_SET_PATH = 'dataset_project_work'
PREPROCESSED_DATA_FILE = 'preprocessed_data.csv'
PRED_FILE = 'pred_data.csv'
SMARTMETER_INCREASE_FILE = 'smartmeter_increase_data.csv'
SMARTMETER_NO_INCREASE_FILE = 'smartmeter_no_increase_data.csv'
FEATURE_DATA_FILE = 'feature_data.csv'
NEEDED_FEATURES = {
    'num_connections': int,
    'annual_consume': int,
    'annual_consume_lowtarif_perc': np.float16,
    'smartmeter_perc': np.float16,
    'city': str
}

FEATURES_AGGREGATE_FUNC = {
    'num_connections': 'sum',
    'annual_consume': 'sum',
    'annual_consume_lowtarif_perc': 'mean',
    'smartmeter_perc': 'mean'
}


def info():
    df = pd.read_csv(PREPROCESSED_DATA_FILE)
    print(df.info())


def describe():
    df = pd.read_csv(PREPROCESSED_DATA_FILE)
    print(df.describe())


def all_preprocess():
    preprocess()
    pred_data()
    smartmeter_data()

def pred_data():
    df = pd.read_csv(PREPROCESSED_DATA_FILE)
    df = df.groupby('year', as_index=False).agg(FEATURES_AGGREGATE_FUNC)
    df.to_csv(PRED_FILE, encoding='utf-8')

def smartmeter_data():
    df = pd.read_csv(PREPROCESSED_DATA_FILE)
    df = df.groupby(['year','city'], as_index=False).agg(FEATURES_AGGREGATE_FUNC)

    df_cities_upper = df.loc[(df['year'] == 2018) & (df['smartmeter_perc'] >= 91.3), ['city']]
    df_cities_lower = df.loc[(df['year'] == 2010) & (df['smartmeter_perc'] < 10), ['city']]
    df_cities_lower_2018 = df.loc[(df['year'] == 2018) & (df['smartmeter_perc'] < 10), ['city']]

    df_cities_with_increase = pd.merge(df_cities_upper, df_cities_lower, how='inner')
    df_cities_no_increase = pd.merge(df_cities_lower_2018, df_cities_lower, how='inner')

    df_increase = pd.merge(df, df_cities_with_increase, how='inner')
    df_no_increase = pd.merge(df, df_cities_no_increase, how='inner')

    df_increase = df_increase.groupby('year', as_index=False).agg(FEATURES_AGGREGATE_FUNC)
    df_no_increase = df_no_increase.groupby('year', as_index=False).agg(FEATURES_AGGREGATE_FUNC)

    df_increase.to_csv(SMARTMETER_INCREASE_FILE, encoding='utf-8')
    df_no_increase.to_csv(SMARTMETER_NO_INCREASE_FILE, encoding='utf-8')


def preprocess():
    print('preprocess')
    folders = ['Electricity', 'Gas']
    files = []
    for folder in folders:
        files += glob.glob(f'{DATA_SET_PATH}/{folder}/*.csv')
    df = pd.DataFrame()
    for file_name in files:
        net_manager = file_name.split('/')[-1].split('_')[0]
        year = file_name[-8:-4]
        if year in ['2009', '2019']:  # Filter out years with incomplete data
            continue
        df_file = pd.read_csv(file_name)
        df_file = df_file[NEEDED_FEATURES.keys()]
        df_file['net_manager'] = net_manager
        df_file['year'] = year
        df = df.append(df_file)

    df = df.astype(NEEDED_FEATURES)
    df.to_csv(PREPROCESSED_DATA_FILE, encoding='utf-8')
    print(df.info())


def pred_model():
    df = pd.read_csv(PRED_FILE)

    x = np.array(df['year']).reshape((-1, 1))
    x_pred = np.arange(2010, 2023).reshape((-1, 1))
    pred_fields = ['annual_consume', 'num_connections', 'smartmeter_perc']

    for pred_field in pred_fields:
        y = np.array(df[pred_field])

        y_true = y.tolist() + [None for _ in range(len(x_pred) - len(y))]
        degrees = [1, 2, 3]

        result_df = pd.DataFrame()
        result_df['year'] = x_pred.flatten()
        result_df['true_values'] = y_true

        for i in range(len(degrees)):
            polynomial_features = PolynomialFeatures(degree=degrees[i],
                                                include_bias=False)
            linear_regression = LinearRegression()
            pipeline = Pipeline([("polynomial_features", polynomial_features),
                                ("linear_regression", linear_regression)])
            pipeline.fit(x, y)
            y_pred = pipeline.predict(x_pred)

            plt.plot(x_pred, y_pred, label=f"Polynomial degree {degrees[i]}", linewidth=4, zorder=i)
            result_df[f'degree_{degrees[i]}'] = y_pred

        plt.scatter(x, y, s=60, label="True values", color='k', zorder=3)
        print(f'result_df {pred_field}: {result_df}')
        plt.title(f'year vs {pred_field}')
        plt.xlabel('year')
        plt.ylabel(pred_field)
        plt.legend(loc="upper left")
        plt.show()


def smartmeter_analyze():
    df_increase = pd.read_csv(SMARTMETER_INCREASE_FILE)
    df_no_increase = pd.read_csv(SMARTMETER_NO_INCREASE_FILE)

    # Normalize on number of connectons
    df_increase['annual_consume_norm'] = df_increase['annual_consume'] / df_increase['num_connections']
    df_no_increase['annual_consume_norm'] = df_no_increase['annual_consume'] / df_no_increase['num_connections']

    _, (ax1, ax2) = plt.subplots(1, 2)
    print(f'df_increase: {df_increase}')
    ax1.scatter(df_increase.year, df_increase.annual_consume_norm, s=60)
    ax1.title.set_text('Smartmeter usage increase')

    print(f'df_no_increase: {df_no_increase}')
    ax2.scatter(df_no_increase.year, df_no_increase.annual_consume_norm, s=60)
    ax2.title.set_text('Smartmeter usage no increase')
    plt.xlabel('year')
    plt.ylabel('annual_consume / num_connections')
    plt.show()

def tariff_comp():
    df = pd.read_csv(PREPROCESSED_DATA_FILE)
    df = df.groupby('year', as_index=False).agg(FEATURES_AGGREGATE_FUNC)
    df['annual_consume_lowtariff'] = df['annual_consume'] * (df['annual_consume_lowtarif_perc'] / 100)
    df['annual_consume_no_lowtariff'] = df['annual_consume'] * (1 - (df['annual_consume_lowtarif_perc'] / 100))

    print(df[['year', 'annual_consume_no_lowtariff','annual_consume_lowtariff']])
    df[['annual_consume_no_lowtariff','annual_consume_lowtariff']].plot(kind='bar')
    plt.xlabel('year')
    plt.ylabel('annual_consume')
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.1),
          fancybox=True, shadow=True, ncol=5)
    plt.show()


@click.command()
@click.argument('command')
def command(command):
    globals()[command]()


if __name__ == '__main__':
    command()