## How to install

This project requires at least python 3.7 to be installed.

    pip install -r requirements.txt

## How to run

### Preprocess

Preprocess needs to be run before all other commands.

    python project.py all_preprocess

### Predictive model

    python project.py pred_model

### Consumption based on smartmeter

    python project.py smartmeter_analyze

### Tariff comparison visualization

    python project.py tariff_comp

## Extra

Link to presentation: https://youtu.be/Yhqn2rdj2yo