\documentclass[oneside,11pt]{article}

% Any additional packages needed should be included after jmlr2e.
% Note that jmlr2e.sty includes epsfig, amssymb, natbib and graphicx,
% and defines many common macros, such as 'proof' and 'example'.
%
% It also sets the bibliographystyle to plainnat; for more information on
% natbib citation styles, see the natbib documentation, a copy of which
% is archived at http://www.jmlr.org/format/natbib.pdf

\usepackage{color}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{float}
\usepackage[utf8]{inputenc}
\usepackage[titletoc,title]{appendix}

\usepackage[a4paper, total={6in, 9in}]{geometry}
\usepackage{dirtytalk}
\usepackage[english]{babel}
\usepackage[backend=biber]{biblatex}
\addbibresource{./ms.bib}

\title{Project (PRO1)}
\author{Adam Nyberg, ang19007}
\date{ 2020-01-08 }

\linespread{1}

\begin{document}

\maketitle

\begin{abstract}
This report answer the process of going from raw data to creating predictive models and doing data analysis. First, we show how to do data exploration and pre-processing of the data. Then the predictive models are developed. The models show that total energy consumption will decline slightly, connected connections will increase, and the percentage of connections using a smart meter is increasing. In addition, it is possible to see that when a connection adds a smart meter the energy consumption goes down on average. This report also show that the percentage of energy consumption during low tariff hours is increasing.
\end{abstract}



\section{Introduction and problem formulation}
This report describes data exploration, data pre-processing, feature construction, how to develop predictive models, and data visualizations. Three different datasets containing data about electricity and gas were used in this report.

The main problem this report will answer is how to do predictive analytics starting from a raw dataset to developing predictive models. Specific tasks that will be described are the following.

\begin{enumerate}
  \item Data exploration to understand the data.
  \item Feature selection and feature extraction.
  \item Develop models to predict energy consumption, number of connections, and smart meter usage.
  \item Visualize tariff consumption in different hours.
\end{enumerate}


\section{Implementation}
The vscode-data-preview\cite{dataPreview} extension to VS Code was used to do the initial data exploration. The goal of data exploration is twofold, understand the data and find bad quality data. First, check what columns exists in the data and their data types. For range data it is important to get a sense of the min, max, median, and average value. In addition it can be helpful to look at the histogram\cite{histo} of the data. The histogram helps with understanding if there are outliers. For categorical values it is important to understand the cardinality\cite{cardi} of the data. Then, data quality issues such as missing data were investigated.

The data was pre-processed by combining all the separate csv files into a single database. Missing values for $net\_manager$ and year were added to the database. The data types for all columns were also set manually. Year $2009$ and $2019$ were removed as they contained incomplete data. To be able to developing the models described in the introduction, the columns $num\_connections$, $annual\_consume$, $annual\_consume\_lowtarif\_perc$, and  $smartmeter\_perc$ were selected. Then the data was grouped by year and table \ref{table:agg_funcs} show which mathematical function was used for the other columns during the aggregation. 

\begin{table}[ht!]
\centering
\begin{tabular}{ |c|c|c|c| }
\hline
$num\_connections$ & $annual\_consume$ & $annual\_consume\_lowtarif\_perc$ & $smartmeter\_perc$ \\
\hline
Sum & Sum & Mean & Mean \\
\hline
\end{tabular}
\caption{Table showing mathematical function used during aggregation per column.}
\label{table:agg_funcs}
\end{table}

This report describes a model, trained on three different data sets, to predict the future energy consumption, number of connections, and usage of smart meters.

\subsection{Model}

The model chosen for future energy consumption prediction is a polynomial regression\cite{polyReg} model. The reason for using polynomial regression is that it is good at extrapolating trends where you have few data points. To calculate the regression\cite{linearReg}, sklearn's $LinearRegression$ together with $PolynomialFeatures$ was used. All parameters were set to the default values. Three different degrees of polynomial features were evaluated to be able to decide a good bias-variance trade-off. 

The model was trained on different data to be able to predict. Table \ref{table:model} shows the data used for the different predictions.

\begin{table}[ht!]
\centering
\begin{tabular}{ |c|c|c|c| }
\hline
Prediction & X Dimension & Y Dimension \\
\hline
P1 Future energy consumption & year & $annual\_consume$  \\
\hline
P2 Number of connections & year & $num\_connections$  \\
\hline
P3 Usage of smart meters & year & $smartmeter\_perc$  \\
\hline
\end{tabular}
\caption{Table shows data used for the different predictions.}
\label{table:model}
\end{table}

\subsection{Smart meter affect on consumption}
To analyze the consumption given the installation of a smart meter it is possible to look at the whole dataset and see some general results. Even better is to look at smaller subsets, for example cities that increased their usage of a smart meter and look at how that affected the amount of consumed energy. To do this, the data was aggregated by $city$ and $year$. Then all cities that increased the use of smart meters from below $10\%$ to above $91.3\%$ were selected to be analyzed. All cities that did not see any increase over $10\%$ were also selected, resulting in two data sets. One data set containing $annual\_consume$ per year for cities that saw a big increase in $smartmeter\_perc$ and one dataset for cities that did not see a big increase in $smartmeter\_perc$. $10\%$ and $91.3\%$ were chosen to get the two data sets to be of equal size and to make sure that the two data sets were different enough to do an analysis on.

\subsection{Regular and low-tariff visualization}
To analyze the comparison of consumption during regular and low tariff hours, a bar chart was used.

\section{Evaluation}
In this section all results are presented.
\subsection{Prediction}
The results for the P1, P2, and P3 models can be seen in figure \ref{fig:m1}, \ref{fig:m2}, and \ref{fig:m3}, respectively.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.8\textwidth]{figs/m1.png}
  \caption{This figure shows three different models for predicting the E1 prediction on $annual\_consume$.}
  \label{fig:m1}
\end{figure}

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.8\textwidth]{figs/m2.png}
  \caption{This figure shows three different models for predicting the E2 prediction on $num\_connections$.}
  \label{fig:m2}
\end{figure}

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.8\textwidth]{figs/m3.png}
  \caption{This figure shows three different models for predicting the E3 prediction on $smartmeter\_perc$.}
  \label{fig:m3}
\end{figure}

\subsection{Smart meter affect on consumption}
Figure \ref{fig:s1} show the result on energy consumption after getting a smart meter.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.8\textwidth]{figs/s1.png}
  \caption{The left plot in this figure show data for cities that saw a big increase in smart meter usage and the right plot show data for cities that did not see a big increase in smart meter usage.}
  \label{fig:s1}
\end{figure}

\subsection{Regular and low-tariff visualization}
Figure \ref{fig:s1} shows the visualization of the consumption comparison between regular and low tariff hours.

\begin{figure}[!ht]
  \centering
  \includegraphics[width=0.8\textwidth]{figs/t1.png}
  \caption{This figure show the consumption comparison.}
  \label{fig:s1}
\end{figure}

\section{Discussion and future work}
The results in general is what would be expected. When looking at the different predictive models for P1, P2, and P3, the model with polynomial degree 2 seems to be performing best in all three cases. This might be because it is a good trade of between underfitting (degree 1) and overfitting (degree 3). Possible future work could be to look at how consumption is changing on a more granular level, such as looking at how the consumption is changing per city and then based on that also do predictions.

When looking at the the result for the smart meter consumption. It is possible to clearly see that the group that installed a smart meter saw a significant decline in annual energy consumption per connection. For the group that did not install a smart meter it is instead possible to see an increase in annual energy consumption per connection. This makes a lot of sense because the smart meter would increase the knowledge of energy consumed and therefore incentives behaviour that reduces the energy consumption. For future work it could be interesting to look at different subsets of the data such as zip code. In addition, it would be interesting to changes the boundaries from $10\%$ and $91.3\%$ to make sure the results are representative for the whole data set.

The results for the visualization of the comparison of consumption in regular and low tariff hours clearly shows that the low tariff consumption is increasing and regular tariff consumption is decreasing. It would also be interesting to change to another visualization and investigate if it is possible to correlation with other features in the data set.

\section{Conclusion and summary}
This report show that energy consumption is slightly declining, the number of connections are increasing, and the percentage of connections with a smart meter is increasing exponentially. It is also possible to conclude that introduction of a smart meter will decrease energy consumption. It is also possible to see that the amount of consumption happening during low tariff hours is increasing.


\pagebreak

\printbibliography

\end{document}